import React from "react"
import Form from "./Form";

class MemeGenerator extends React.Component {
    constructor() {
        super();
        this.state = {
            image:          "http://i.imgflip.com/1bij.jpg",
            memeImages:     [],
            childData:      {}
        };
        this.getRandomImg = this.getRandomImg.bind(this);
    }

    componentDidMount() {
        fetch("https://api.imgflip.com/get_memes")
            .then(response =>  response.json())
            .then(responseData =>  {
                this.setState({
                    memeImages:  responseData.data.memes
                })
            });
    }

    showChildData = (childData) =>  {
        console.log("Data from child: ", childData);
        this.setState({
            childData:  childData
        })
        this.getRandomImg();
    };

    getRandomImg() {
        const rand = Math.floor(Math.random() * this.state.memeImages.length);
        this.setState({
            image:  this.state.memeImages[rand].url
        })
    }

    render() {
        return (
            <div>
                <h3>Meme Generator</h3>
                <Form childData={this.showChildData}/>
                <div style={{position: "relative"}}>
                    <img src={this.state.image} alt="meme" height="600px" />
                    <h2 className="top-text">{this.state.childData.topText}</h2>
                    <h2 className="bottom-text">{this.state.childData.bottomText}</h2>
                </div>
            </div>
        )
    }
}

export default MemeGenerator

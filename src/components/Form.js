import React from "react"

class Form extends React.Component {
    constructor() {
        super();
        this.state = {
            topText:        "",
            bottomText:     "",
        };
        this.inputChange    =   this.inputChange.bind(this);
        this.submitForm     =   this.submitForm.bind(this);
    }

    // if we use arrow function we do not have to bind methods in constructor
    inputChange(event) {
        const {name, value}    =   event.target;
        this.setState({
            [name]:     value
        });
    }

    submitForm(event) {
        // prevents page refresh
        event.preventDefault();
        // passing data to parent component
        this.props.childData(this.state);
    }

    render() {
        return (
            <div>
                <form onSubmit={this.submitForm}>
                    <div>
                        <input type="text" name="topText" value={this.state.topText} onChange={this.inputChange} placeholder="Top text" />
                    </div>
                    <div>
                        <input type="text" name="bottomText" value={this.state.bottomText} onChange={this.inputChange} placeholder="Bottom text" />
                    </div>
                    <input type="submit" />
                </form>
            </div>
        )
    }
}

export default Form

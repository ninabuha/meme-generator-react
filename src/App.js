import './App.css';
import MemeGenerator from "./components/MemeGenerator";
import HeaderDisplay from "./components/HeaderDisplay";
import React from "react"

function App() {
  return (
    <div className="App">
        <HeaderDisplay />
        <MemeGenerator />
    </div>
  );
}

export default App;
